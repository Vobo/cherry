<?php

namespace Cherry\AppBundle\Model;

use Money\Money;

class GambleModel
{
    const STATUS_WON = 'won';
    const STATUS_LOST = 'lost';

    /** @var string */
    protected $status;

    /** @var Money */
    protected $winnings;

    /** @var MoneyTransferModel */
    protected $wager;

    private function __construct($status, MoneyTransferModel $wager, Money $winnings)
    {
        $this->status = $status;
        $this->wager = $wager;
        $this->winnings = $winnings;
    }

    /**
     * @param MoneyTransferModel $wager
     * @param Money $winnings
     * @return self
     */
    public static function won(MoneyTransferModel $wager, Money $winnings)
    {
        $model = new self(self::STATUS_WON, $wager, $winnings);

        return $model;
    }

    /**
     * @param MoneyTransferModel $wager
     * @return self
     */
    public static function lost(MoneyTransferModel $wager)
    {
        $model = new self(self::STATUS_LOST, $wager, new Money(0, $wager->getMoney()->getCurrency()));

        return $model;
    }

    /**
     * @return bool
     */
    public function isWon()
    {
        return self::STATUS_WON === $this->status;
    }

    /**
     * @return Money
     */
    public function getWageredMoney()
    {
        return $this->wager->getMoney();
    }

    /**
     * @return Money
     */
    public function getWinnings()
    {
        return $this->winnings;
    }

    /**
     * @return PortfolioModel
     */
    public function getPortfolio()
    {
        return $this->wager->getPortfolio();
    }
}
