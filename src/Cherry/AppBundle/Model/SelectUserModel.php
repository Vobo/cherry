<?php

namespace Cherry\AppBundle\Model;

use Cherry\AppBundle\Entity\User;

class SelectUserModel
{
    /** @var User */
    protected $user;

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
