<?php

namespace Cherry\AppBundle\Model\Bonus;

use Cherry\AppBundle\Entity\Bonus;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;

abstract class BaseBonusModel implements BonusModelInterface
{
    /** @var Bonus */
    protected $bonus;

    /** @var RewardModel */
    protected $reward;

    public function __construct(Bonus $bonus)
    {
        $this->bonus = $bonus;
        $this->reward = new RewardModel($bonus);
    }

    /**
     * {@inheritdoc}
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @return string
     *
     * @Assert\NotBlank()
     */
    public function getName()
    {
        return $this->bonus->getName();
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->bonus->setName($name);

        return $this;
    }

    /**
     * @return RewardModel
     *
     * @Assert\Valid()
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * @return string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"active", "disabled"})
     */
    public function getStatus()
    {
        return $this->bonus->getStatus();
    }

    /**
     * @param $status
     * @return $this
     * @throws Exception
     */
    public function setStatus($status)
    {
        $this->bonus->setStatus($status);

        return $this;
    }

    /**
     * @return int
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     */
    public function getWagerMultiplier()
    {
        return $this->bonus->getWagerMultiplier();
    }

    /**
     * @param int $multiplier
     * @return $this
     */
    public function setWagerMultiplier($multiplier)
    {
        $this->bonus->setWagerMultiplier($multiplier);

        return $this;
    }
}
