<?php

namespace Cherry\AppBundle\Model\Bonus;

use Cherry\AppBundle\Entity\Bonus;

// Just a marker
interface BonusModelInterface
{
    /**
     * @return Bonus
     */
    public function getBonus();
}
