<?php

namespace Cherry\AppBundle\Model\Bonus;

use Cherry\AppBundle\Entity\Bonus;
use Exception;

class LoginBonusModel extends BaseBonusModel
{
    const TYPE = 'login';

    /** @var Bonus */
    protected $bonus;

    /** @var RewardModel */
    protected $reward;

    public function __construct(Bonus $bonus)
    {
        $this->bonus = $bonus;
        $this->reward = new RewardModel($bonus);

        if (self::TYPE !== $bonus->getType()) {
            throw new Exception(sprintf(
                'Bonus of invalid type passed, got "", expected "".',
                $bonus->getType(),
                self::TYPE
            ));
        }
    }

    /**
     * @return RewardModel
     */
    public function getReward()
    {
        return $this->reward;
    }
}
