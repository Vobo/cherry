<?php

namespace Cherry\AppBundle\Model\Bonus;

use Cherry\AppBundle\Entity\Bonus;
use Symfony\Component\Validator\Constraints as Assert;

class RewardModel
{
    const TYPE_FIXED = 'fixed';
    const TYPE_PERCENTAGE = 'percentage';

    /** @var Bonus */
    protected $bonus;

    public function __construct(Bonus $bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * @return string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"fixed", "percentage"})
     */
    public function getRewardType()
    {
        return $this->bonus->getRewardType();
    }

    /**
     * @param string $rewardType
     * @return $this
     */
    public function setRewardType($rewardType)
    {
        $this->bonus->setRewardType($rewardType);

        return $this;
    }

    /**
     * @return int
     *
     * @Assert\NotBlank()
     * @Assert\Expression(
     *   "this.getRewardType() != 'percentage' || value > 0 and value <= 100",
     *   message = "Value has to be a number in 1-100 range."
     * )
     * @Assert\Expression(
     *   "this.getRewardType() != 'fixed' || value > 0",
     *   message = "Value has to be a positive integer."
     * )
     */
    public function getRewardValue()
    {
        return $this->bonus->getRewardValue();
    }

    /**
     * @param int $rewardValue
     * @return $this
     */
    public function setRewardValue($rewardValue)
    {
        $this->bonus->setRewardValue($rewardValue);

        return $this;
    }
}
