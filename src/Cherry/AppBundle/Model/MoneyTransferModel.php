<?php

namespace Cherry\AppBundle\Model;

use Money\Money;
use Symfony\Component\Validator\Constraints as Assert;

class MoneyTransferModel
{
    const VALIDATION_GROUP_DEPOSIT = 'deposit';
    const VALIDATION_GROUP_WITHDRAWAL = 'withdrawal';

    /**
     * @var Money
     *
     * @Assert\Expression(
     *   "value.isPositive()",
     *   message = "Transferred money has to be a positive integer.",
     *   groups = {"deposit", "withdrawal"}
     * )
     * @Assert\Expression(
     *   "value.equals(this.getPortfolio().getTotalMoney()) || value.lessThan(this.getPortfolio().getTotalMoney())",
     *   message = "Cannot withdraw over the account balance.",
     *   groups = {"deposit", "withdrawal"}
     * )
     */
    protected $money;

    /** @var PortfolioModel */
    protected $portfolio;

    public function __construct(
        PortfolioModel $portfolio
    ) {
        $this->portfolio = $portfolio;
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function setMoney(Money $money)
    {
        $this->money = $money;

        return $this;
    }

    /**
     * @return Money
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * @return PortfolioModel
     */
    public function getPortfolio()
    {
        return $this->portfolio;
    }
}
