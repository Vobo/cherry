<?php

namespace Cherry\AppBundle\Model;

use Cherry\AppBundle\Entity\BonusWallet;
use Cherry\AppBundle\Entity\MoneyWallet;
use Cherry\AppBundle\Entity\User;
use Doctrine\Common\Collections\Collection;
use Money\Currency;
use Money\Money;

class PortfolioModel
{
    /** @var User */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Money
     */
    public function getTotalMoney()
    {
        return $this->getMoney()->add($this->getBonusMoney());
    }

    /**
     * @return Money
     */
    public function getMoney()
    {
        return $this->user->getMoneyWallet()->getCurrentMoney();
    }

    /**
     * @return Money
     */
    public function getBonusMoney()
    {
        $bonusWallets = $this->getBonusActiveWallets();
        /** @var Money $money */
        $money = new Money(0, new Currency('EUR'));

        foreach ($bonusWallets as $bonusWallet) {
            $money = $money->add($bonusWallet->getCurrentMoney());
        }

        return $money;
    }

    /**
     * @return MoneyWallet
     */
    public function getMoneyWallet()
    {
        return $this->user->getMoneyWallet();
    }

    /**
     * @return Collection|BonusWallet[] Active bonus wallets
     */
    public function getBonusActiveWallets()
    {
        $activeBonusWallets = $this
            ->user
            ->getBonusWallets()
            ->filter(function (BonusWallet $bonusWallet) {
                return BonusWallet::STATUS_ACTIVE === $bonusWallet->getStatus();
            });

        return $activeBonusWallets;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
