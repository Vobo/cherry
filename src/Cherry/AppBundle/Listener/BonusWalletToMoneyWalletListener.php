<?php

namespace Cherry\AppBundle\Listener;

use Cherry\AppBundle\Entity\BonusWallet;
use Cherry\AppBundle\Model\PortfolioModel;
use Cherry\AppBundle\Service\Bank\Event\BankEvents;
use Cherry\AppBundle\Service\Bank\Event\BankGambleEvent;
use Money\Money;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BonusWalletToMoneyWalletListener implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            BankEvents::EVENT_PROCESSED_GAMBLE => 'updateBonusWalletsWithWageredMoney',
        ];
    }

    /**
     * @param BankGambleEvent $event
     */
    public function updateBonusWalletsWithWageredMoney(BankGambleEvent $event)
    {
        $bonusWallets = $event->getGamble()->getPortfolio()->getBonusActiveWallets();
        $wageredMoney = $event->getGamble()->getWageredMoney();

        foreach ($bonusWallets as $bonusWallet) {
            $bonusWallet->addWageredMoney($wageredMoney);
            $wagerRequirement = $bonusWallet->getInitialMoney()->multiply($bonusWallet->getBonus()->getWagerMultiplier());

            if ($bonusWallet->getWageredMoney()->equals($wagerRequirement)
                || $bonusWallet->getWageredMoney()->greaterThan($wagerRequirement)
            ) {
                $this->convertBonusWalletToMoneyInMoneyWallet($event->getGamble()->getPortfolio(), $bonusWallet);
            }
        }
    }

    /**
     * @param PortfolioModel $portfolio
     * @param BonusWallet $bonusWallet
     */
    protected function convertBonusWalletToMoneyInMoneyWallet(PortfolioModel $portfolio, BonusWallet $bonusWallet)
    {
        // @todo Logs, events

        $moneyWallet = $portfolio->getMoneyWallet();
        $bonusMoney = $bonusWallet->getCurrentMoney();
        $moneyWallet->setCurrentMoney($moneyWallet->getCurrentMoney()->add($bonusMoney));
        $bonusWallet->setCurrentMoney(new Money(0, $bonusMoney->getCurrency()));
        $bonusWallet->setStatus(BonusWallet::STATUS_CONVERTED);
    }

}
