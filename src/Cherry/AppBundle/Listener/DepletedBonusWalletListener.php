<?php

namespace Cherry\AppBundle\Listener;

use Cherry\AppBundle\Entity\BonusWallet;
use Cherry\AppBundle\Service\Bank\Event\BankEvents;
use Cherry\AppBundle\Service\Bank\Event\BankGambleEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DepletedBonusWalletListener implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [BankEvents::EVENT_PROCESSED_GAMBLE => ['checkForDepletedBonusWallets', 2]];
    }

    public function checkForDepletedBonusWallets(BankGambleEvent $event)
    {
        if ($event->getGamble()->isWon()) {
            return;
        }

        foreach ($event->getGamble()->getPortfolio()->getBonusActiveWallets() as $bonusWallet) {
            if ($bonusWallet->getCurrentMoney()->isZero()) {
                $bonusWallet->setStatus(BonusWallet::STATUS_DEPLETED);
            }
        }
    }

}
