<?php

namespace Cherry\AppBundle\Listener;

use Cherry\AppBundle\Entity\Bonus;
use Cherry\AppBundle\Entity\BonusWallet;
use Cherry\AppBundle\Entity\User;
use Cherry\AppBundle\Model\Bonus\LoginBonusModel;
use Cherry\AppBundle\Repository\BonusRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Money\Currency;
use Money\Money;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class TriggerOnLoginBonus implements EventSubscriberInterface
{
    /** @var ObjectManager */
    protected $objectManager;

    public function __construct(
        ObjectManager $objectManager
    ) {
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [SecurityEvents::INTERACTIVE_LOGIN => 'triggerBonusOnLogin'];
    }

    public function triggerBonusOnLogin(InteractiveLoginEvent $event)
    {
        $bonusRepository = $this->objectManager->getRepository('CherryAppBundle:Bonus');
        /** @var Bonus[] $bonuses */
        $bonuses = $bonusRepository->findBy([
            'status' => Bonus::STATUS_ACTIVE,
            'type' => LoginBonusModel::TYPE,
        ]);

        if (!$bonuses) {
            return;
        }

        /** @var User $user */
        $user = $event->getAuthenticationToken()->getUser();

        foreach ($bonuses as $bonus) {
            // Quick, dirty hack since there's no time left :(
            $money = new Money($bonus->getRewardValue()*100, new Currency('EUR'));
            $bonusWallet = new BonusWallet();
            $bonusWallet
                ->setBonus($bonus)
                ->setInitialMoney($money)
                ->setCurrentMoney($money);

            $user->addBonusWallet($bonusWallet);
        }

        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }
}
