<?php

namespace Cherry\AppBundle\Controller;

use Cherry\AppBundle\Entity\Bonus;
use Cherry\AppBundle\Model\Bonus\BonusModelInterface;
use Cherry\AppBundle\Model\Bonus\LoginBonusModel;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/bonus")
 */
class BonusController extends Controller
{
    /**
     * Lists all Bonus entities.
     *
     * @Route("/", name="bonus")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CherryAppBundle:Bonus')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/new/login")
     * @Method("GET")
     * @Template("CherryAppBundle:Bonus:new.html.twig")
     */
    public function newLoginBonusAction()
    {
        $form = $this->createCreateForm(LoginBonusModel::TYPE);

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/new/login")
     * @Method("POST")
     * @Template("CherryAppBundle:Bonus:new.html.twig")
     */
    public function createLoginBonusAction(Request $request)
    {
        $form = $this->createCreateForm(LoginBonusModel::TYPE);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var BonusModelInterface $bonusModel */
            $bonusModel = $form->getData();
            $em->persist($bonusModel->getBonus());
            $em->flush();
            $this->get('braincrafted_bootstrap.flash')->success('Bonus created.');

            return $this->redirect($this->generateUrl('bonus_show', array('id' => $bonusModel->getBonus()->getId())));
        }

        return array(
            'form'   => $form->createView(),
        );
    }

    /**
     * @param string $type
     * @return Form
     * @throws Exception
     */
    protected function createCreateForm($type)
    {
        $factoryCollection = $this->get('cherry_app_bundle.bonus.factory.collection');
        $factory = $factoryCollection->getFactoryForType($type);

        $form = $this->createForm($factory->createFormType(), $factory->createModelWithEntity(), array(
            'action' => $this->generateUrl('cherry_app_bonus_createloginbonus'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * @Route("/{id}", name="bonus_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CherryAppBundle:Bonus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bonus entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @Route("/{id}/edit", name="bonus_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CherryAppBundle:Bonus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bonus entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @param Bonus $bonus
     * @return Form
     */
    public function createEditForm(Bonus $bonus)
    {
        $factoryCollection = $this->get('cherry_app_bundle.bonus.factory.collection');
        $factory = $factoryCollection->getFactoryForBonus($bonus);
        $model = $factory->createModel($bonus);

        $form = $this->createForm($factory->createFormType(), $model, array(
            'action' => $this->generateUrl('bonus_update', ['id' => $bonus->getId()]),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Edit'));

        return $form;
    }

    /**
     * @Route("/{id}", name="bonus_update")
     * @Method("PUT")
     * @Template("CherryAppBundle:Bonus:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CherryAppBundle:Bonus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bonus entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('braincrafted_bootstrap.flash')->success('Bonus updated.');

            return $this->redirect($this->generateUrl('bonus_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * @Route("/{id}", name="bonus_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CherryAppBundle:Bonus')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Bonus entity.');
            }

            $bonusWalletRepository = $this->getDoctrine()->getRepository('CherryAppBundle:BonusWallet');
            $count = $bonusWalletRepository->countWalletsUsing($entity);

            if ($count) {
                $this->get('braincrafted_bootstrap.flash')->error('Cannot remove bonus with bonus wallets assigned to it.');
            } else {
                $em->remove($entity);
                $em->flush();
                $this->get('braincrafted_bootstrap.flash')->success('Bonus removed.');
            }
        }

        return $this->redirect($this->generateUrl('bonus'));
    }

    /**
     * @return Form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bonus_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
