<?php

namespace Cherry\AppBundle\Controller;

use Cherry\AppBundle\Entity\User;
use Cherry\AppBundle\Form\MoneyTransferType;
use Cherry\AppBundle\Form\SelectUserType;
use Cherry\AppBundle\Model\MoneyTransferModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SimulationController extends Controller
{
    /**
     * @Route("/simulation")
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $data = [
            'select_user_form' => $this->createSelectUserForm()->createView(),
        ];

        if ($this->get('cherry_app_bundle.security.selected_user_storage')->hasSelectedUser()) {
            $data = array_merge($data, [
                'deposit_form' => $this->createDepositForm()->createView(),
                'gamble_form' => $this->createGambleForm()->createView(),
            ]);
        }

        return $data;
    }

    /**
     * @Route("/simulation/select_user", name="select_user")
     * @Method("POST")
     */
    public function selectUserAction(Request $request)
    {
        $form = $this->createSelectUserForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var User $user */
            $user = $form->getData()->getUser();
            $this->get('cherry_app_bundle.security.selected_user_storage')->setSelectedUser($user);
            $this->get('braincrafted_bootstrap.flash')->info(sprintf('Selected "%s" user.', $user->getUsername()));
        }

        return $this->redirect($this->generateUrl('cherry_app_simulation_index'));
    }

    /**
     * @Template()
     */
    public function balanceAction()
    {
        $user = $this->get('cherry_app_bundle.security.selected_user_storage')->getSelectedUser();
        $portfolio = $user
            ? $this->get('cherry_app_bundle.model.factory.portfolio_model')->createModel($user)
            : null;

        return ['portfolio' => $portfolio];
    }

    /**
     * @Route("/simulation/deposit")
     * @Method("POST")
     * @Template("CherryAppBundle:Simulation:index.html.twig")
     */
    public function depositAction(Request $request)
    {
        $form = $this->createDepositForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var MoneyTransferModel $model */
            $model = $form->getData();

            $this->get('cherry_app_bundle.bank')->depositMoney($model);
            $this->get('braincrafted_bootstrap.flash')->info('Money deposited');

            $user = $model->getPortfolio()->getUser();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('cherry_app_simulation_index'));
        }

        return [
            'select_user_form' => $this->createSelectUserForm()->createView(),
            'deposit_form' => $form->createView(),
            'gamble_form' => $this->createGambleForm()->createView(),
        ];
    }

    /**
     * @Route("/simulation/gamble")
     * @Method("POST")
     * @Template("CherryAppBundle:Simulation:index.html.twig")
     */
    public function gambleAction(Request $request)
    {
        $form = $this->createGambleForm();
        $form->handleRequest($request);
        $wonMoney = null;

        if ($form->isValid()) {
            /** @var MoneyTransferModel $model */
            $model = $form->getData();
            $gamble = $this->get('cherry_app_bundle.machine.default')->gamble($model);
            $this->get('cherry_app_bundle.bank')->processGamble($gamble);

            $flashMessage = $this->get('braincrafted_bootstrap.flash');
            $gamble->isWon()
                ? $flashMessage->success('Won')
                : $flashMessage->error('Lost');

            $user = $model->getPortfolio()->getUser();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $wonMoney = $gamble->getWinnings();
        }

        return [
            'select_user_form' => $this->createSelectUserForm()->createView(),
            'deposit_form' => $this->createDepositForm()->createView(),
            'gamble_form' => $form->createView(),
            'wonMoney' => $wonMoney,
        ];
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function createSelectUserForm()
    {
        $user = $this->get('cherry_app_bundle.security.selected_user_storage')->getSelectedUser();
        $model = $this->get('cherry_app_bundle.model.factory.select_user_model')->createModel($user);
        $form = $this->createForm(new SelectUserType(), $model, array(
            'action' => $this->generateUrl('select_user'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Select'));

        return $form;
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function createDepositForm()
    {
        $user = $this->get('cherry_app_bundle.security.selected_user_storage')->getSelectedUser();
        $model = $this->get('cherry_app_bundle.model.factory.money_transfer_model')->createModelForUser($user);

        $form = $this->createForm(new MoneyTransferType(), $model, array(
            'action' => $this->generateUrl('cherry_app_simulation_deposit'),
            'method' => 'POST',
            'validation_groups' => [MoneyTransferModel::VALIDATION_GROUP_DEPOSIT],
        ));

        $form->add('submit', 'submit', array('label' => 'Deposit'));

        return $form;
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function createGambleForm()
    {
        $user = $this->get('cherry_app_bundle.security.selected_user_storage')->getSelectedUser();
        $model = $this->get('cherry_app_bundle.model.factory.money_transfer_model')->createModelForUser($user);

        $form = $this->createForm(new MoneyTransferType(), $model, array(
            'action' => $this->generateUrl('cherry_app_simulation_gamble'),
            'method' => 'POST',
            'validation_groups' => [MoneyTransferModel::VALIDATION_GROUP_WITHDRAWAL],
        ));

        $form->add('submit', 'submit', array('label' => 'Gamble'));

        return $form;
    }
}
