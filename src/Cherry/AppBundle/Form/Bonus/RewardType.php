<?php

namespace Cherry\AppBundle\Form\Bonus;

use Cherry\AppBundle\Model\Bonus\RewardModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RewardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [];
        foreach ($options['allowed_reward_types'] as $rewardType) {
            $choices[$rewardType] = ucfirst($rewardType);
        }

        $builder
            ->add('reward_type', 'choice', [
                'choices' => $choices,
            ])
            ->add('reward_value', 'number')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'data_class' => 'Cherry\AppBundle\Model\Bonus\RewardModel',
                'allowed_reward_types' => [RewardModel::TYPE_FIXED, RewardModel::TYPE_PERCENTAGE],
            ))
            ->setAllowedTypes([
                'allowed_reward_types' => 'array',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'cherry_appbundle_bonus_reward';
    }
}
