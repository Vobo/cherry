<?php

namespace Cherry\AppBundle\Form\Bonus;

use Cherry\AppBundle\Model\Bonus\RewardModel;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoginBonusType extends BaseBonusType
{
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cherry\AppBundle\Model\Bonus\LoginBonusModel',
            'allowed_reward_types' => [RewardModel::TYPE_FIXED],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cherry_appbundle_bonus_login';
    }
}
