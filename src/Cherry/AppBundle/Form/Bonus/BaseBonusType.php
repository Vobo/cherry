<?php

namespace Cherry\AppBundle\Form\Bonus;

use Cherry\AppBundle\Entity\Bonus;
use Cherry\AppBundle\Model\Bonus\RewardModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

abstract class BaseBonusType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('reward', new RewardType(), [
                'allowed_reward_types' => $options['allowed_reward_types'],
            ])
            ->add('status', 'choice', [
                'choices' => [
                    Bonus::STATUS_ACTIVE => 'Active',
                    Bonus::STATUS_DISABLED => 'Disabled',
                ],
            ])
            ->add('wager_multiplier', 'number')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'allowed_reward_types' => [RewardModel::TYPE_FIXED, RewardModel::TYPE_PERCENTAGE],
            ])
            ->setAllowedTypes([
                'allowed_reward_types' => 'array',
            ]);
    }

}
