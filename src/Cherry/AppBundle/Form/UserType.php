<?php

namespace Cherry\AppBundle\Form;

use Cherry\AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('name')
            ->add('lastName')
            ->add('age')
            ->add('gender', 'choice', [
                'choices' => [
                    User::GENDER_MALE => 'Male',
                    User::GENDER_FEMALE => 'Female',
                ]
            ])
            ->add('moneyWallet', new MoneyWalletType())
            ->add('bonusWallets', 'bootstrap_collection', [
                'type'               => new BonusWalletType(),
                'allow_add'          => true,
                'allow_delete'       => true,
                'add_button_text'    => 'Add bonus wallet',
                'delete_button_text' => 'Remove bonus wallet',
                'sub_widget_col'     => 9,
                'button_col'         => 3,
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cherry\AppBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cherry_appbundle_user';
    }
}
