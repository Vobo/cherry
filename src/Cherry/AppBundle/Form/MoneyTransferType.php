<?php

namespace Cherry\AppBundle\Form;

use Cherry\AppBundle\Model\MoneyTransferModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MoneyTransferType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Money', 'tbbc_money')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'Cherry\AppBundle\Model\MoneyTransferModel'
            ])
            ->setRequired([
                'validation_groups',
            ])
            ->setAllowedValues([
                'validation_groups' => [[MoneyTransferModel::VALIDATION_GROUP_DEPOSIT], [MoneyTransferModel::VALIDATION_GROUP_WITHDRAWAL]]
            ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cherry_appbundle_money_transfer';
    }
}
