<?php

namespace Cherry\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MoneyWalletType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('initialMoney', 'tbbc_money')
            ->add('currentMoney', 'tbbc_money')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cherry\AppBundle\Entity\MoneyWallet'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cherry_appbundle_money_wallet';
    }
}
