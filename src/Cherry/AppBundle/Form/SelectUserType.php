<?php

namespace Cherry\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SelectUserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'entity', [
                'class' => 'Cherry\AppBundle\Entity\User',
                'property' => 'username',
                'empty_value' => 'Select user',
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cherry\AppBundle\Model\SelectUserModel'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cherry_appbundle_select_user';
    }
}
