<?php

namespace Cherry\AppBundle\Form;

use Cherry\AppBundle\Entity\BonusWallet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BonusWalletType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('initialMoney', 'tbbc_money')
            ->add('currentMoney', 'tbbc_money')
            ->add('bonus', 'entity', [
                'class' => 'Cherry\AppBundle\Entity\Bonus',
                'property' => 'name',
                'empty_value' => 'Select bonus',
            ])
            ->add('status', 'choice', [
                'choices' => [
                    BonusWallet::STATUS_ACTIVE => 'Active',
                    BonusWallet::STATUS_DEPLETED => 'Depleted',
                ],
                'disabled' => true,
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'Cherry\AppBundle\Entity\BonusWallet',
            ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cherry_appbundle_money_wallet';
    }
}
