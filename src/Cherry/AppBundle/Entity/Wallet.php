<?php

namespace Cherry\AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Gedmo\Mapping\Annotation as Gedmo;
use Money\Currency;
use Money\Money;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="wallet")
 * @ORM\Entity(repositoryClass="Cherry\AppBundle\Repository\WalletRepository")
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap({"moneyWallet" = "MoneyWallet", "bonusWallet" = "BonusWallet"})
 */
abstract class Wallet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="initial_money", type="integer")
     * @Assert\NotNull()
     * @Assert\GreaterThanOrEqual(0)
     */
    protected $initialMoney;

    /**
     * @var integer
     *
     * @ORM\Column(name="current_money", type="integer")
     * @Assert\NotNull()
     * @Assert\GreaterThanOrEqual(0)
     */
    protected $currentMoney;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->initialMoney = 0;
        $this->currentMoney = 0;
    }

    /**
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Money $initialMoney
     * @return $this
     */
    public function setInitialMoney(Money $initialMoney)
    {
        $this->initialMoney = $initialMoney->getAmount();

        return $this;
    }

    /**
     * @return Money
     */
    public function getInitialMoney()
    {
        return new Money($this->initialMoney, new Currency('EUR'));
    }

    /**
     * @param Money $currentMoney
     *
     * @return $this
     */
    public function setCurrentMoney(Money $currentMoney)
    {
        $this->currentMoney = $currentMoney->getAmount();

        return $this;
    }

    /**
     * @return Money
     */
    public function getCurrentMoney()
    {
        return new Money($this->currentMoney, new Currency('EUR'));
    }

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
