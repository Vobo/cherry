<?php

namespace Cherry\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Cherry\AppBundle\Repository\UserRepository")
 * @UniqueEntity("username")
 */
class User implements UserInterface
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $username;

    // @todo Do something about it
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    protected $password;

    // @todo Do something about it
    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=true)
     */
    protected $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    protected $lastName;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     * @Assert\LessThanOrEqual(150)
     */
    protected $age;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=6)
     * @Assert\Choice(choices = {"male", "female"}, message="Choose a valid gender")
     */
    protected $gender;

    /**
     * @var MoneyWallet
     *
     * @ORM\OneToOne(targetEntity="MoneyWallet", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="money_wallet_id", referencedColumnName="id")
     * @Assert\NotNull()
     * @Assert\Valid()
     */
    protected $moneyWallet;

    /**
     * @var Collection|BonusWallet[]
     *
     * @ORM\ManyToMany(targetEntity="Wallet", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinTable(name="user_to_bonus_wallet",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="wallet_id", referencedColumnName="id", unique=true)}
     * )
     * @Assert\Valid()
     */
    protected $bonusWallets;

    public function __construct()
    {
        $this->bonusWallets = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        // No sensitive data is kept here
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $salt
     * @return $this
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param integer $age
     * @return $this
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param string $gender
     * @return $this
     * @throws Exception
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param MoneyWallet $moneyWallet
     * @return $this
     */
    public function setMoneyWallet(MoneyWallet $moneyWallet)
    {
        $this->moneyWallet = $moneyWallet;

        return $this;
    }

    /**
     * @return MoneyWallet
     */
    public function getMoneyWallet()
    {
        return $this->moneyWallet;
    }

    /**
     * @param BonusWallet $bonusWallet
     * @return $this
     */
    public function addBonusWallet(BonusWallet $bonusWallet)
    {
        $this->bonusWallets->add($bonusWallet);

        return $this;
    }

    /**
     * @return Collection|BonusWallet[]
     */
    public function getBonusWallets()
    {
        return $this->bonusWallets;
    }

}
