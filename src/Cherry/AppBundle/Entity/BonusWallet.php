<?php

namespace Cherry\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Exception;
use Money\Currency;
use Money\Money;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Cherry\AppBundle\Repository\BonusWalletRepository")
 */
class BonusWallet extends Wallet
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DEPLETED = 'depleted';
    const STATUS_CONVERTED = 'converted';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    protected $status;

    /**
     * @var Bonus
     *
     * @ORM\ManyToOne(targetEntity="Bonus")
     * @ORM\JoinColumn(name="bonus_id", referencedColumnName="id")
     *
     * @Assert\NotNull()
     */
    protected $bonus;

    /**
     * Money wagered when bonus wallet was in place.
     *
     * @var Money
     *
     * @ORM\Column(name="wagered_money", type="integer")
     */
    protected $wageredMoney;

    public function __construct()
    {
        parent::__construct();
        $this->status = self::STATUS_ACTIVE;
        $this->wageredMoney = 0;
    }

    public function setCurrentMoney(Money $currentMoney)
    {
        return parent::setCurrentMoney($currentMoney);
    }

    public function setStatus($status)
    {
        if (!in_array($status, [self::STATUS_DEPLETED, self::STATUS_ACTIVE, self::STATUS_CONVERTED])) {
            throw new Exception("Invalid status '$status'.");
        }

        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param Bonus $bonus
     * @return $this
     */
    public function setBonus(Bonus $bonus)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * @return Bonus
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param Money $wageredMoney
     * @return $this
     */
    public function addWageredMoney(Money $wageredMoney)
    {
        $this->wageredMoney = $this->getWageredMoney()->add($wageredMoney)->getAmount();

        return $this;
    }

    /**
     * @return Money
     */
    public function getWageredMoney()
    {
       return new Money($this->wageredMoney, new Currency('EUR'));
    }
}
