<?php

namespace Cherry\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="bonus")
 * @ORM\Entity(repositoryClass="Cherry\AppBundle\Repository\BonusRepository")
 * @UniqueEntity("name")
 */
class Bonus
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="event", type="string", length=255)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="reward_type", type="string", length=255)
     */
    protected $rewardType;

    /**
     * @var integer
     *
     * @ORM\Column(name="reward_value", type="integer")
     */
    protected $rewardValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="wager_multiplier", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(1)
     * @Assert\LessThanOrEqual(100)
     */
    protected $wagerMultiplier;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="requirements", type="text", nullable=true)
     */
    protected $requirements;

    public function __construct()
    {
        $this->status = self::STATUS_ACTIVE;
        $this->rewardValue = 0;
        $this->wagerMultiplier = 1;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $rewardType
     * @return $this
     */
    public function setRewardType($rewardType)
    {
        $this->rewardType = $rewardType;

        return $this;
    }

    /**
     * @return string
     */
    public function getRewardType()
    {
        return $this->rewardType;
    }

    /**
     * @param string $rewardValue
     * @return $this
     */
    public function setRewardValue($rewardValue)
    {
        $this->rewardValue = $rewardValue;

        return $this;
    }

    /**
     * @return string
     */
    public function getRewardValue()
    {
        return $this->rewardValue;
    }

    /**
     * @param integer $wagerMultiplier
     * @return $this
     */
    public function setWagerMultiplier($wagerMultiplier)
    {
        $this->wagerMultiplier = $wagerMultiplier;

        return $this;
    }

    /**
     * @return integer
     */
    public function getWagerMultiplier()
    {
        return $this->wagerMultiplier;
    }

    /**
     * @param string $status
     * @return $this
     * @throws Exception
     */
    public function setStatus($status)
    {
        if (!in_array($status, [self::STATUS_ACTIVE, self::STATUS_DISABLED])) {
            throw new Exception('Invalid status passed.');
        }

        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $requirements
     *
     * @return $this
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * @return $this
     */
    public function getRequirements()
    {
        return $this->requirements;
    }
}
