<?php

namespace Cherry\AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class BonusFactoryCollectionCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('cherry_app_bundle.bonus.factory.collection')) {
            return;
        }

        $factoryCollection = $container->getDefinition('cherry_app_bundle.bonus.factory.collection');
        $factories = $container->findTaggedServiceIds('cherry_app_bundle.bonus.factory');

        foreach ($factories as $factoryId => $factoryAttributes) {
            $factoryCollection->addMethodCall('registerFactory', [new Reference($factoryId)]);
        }
    }
}
