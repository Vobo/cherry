<?php

namespace Cherry\AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class BankCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('cherry_app_bundle.simple_bank')) {
            return;
        }

        $bankDefinition = $container->getDefinition('cherry_app_bundle.simple_bank');
        $handlers = $container->findTaggedServiceIds('cherry_app_bundle.bank.handler');

        foreach ($handlers as $handlerId => $handlerAttributes) {
            $bankDefinition->addMethodCall('registerHandler', [new Reference($handlerId)]);
        }
    }
}
