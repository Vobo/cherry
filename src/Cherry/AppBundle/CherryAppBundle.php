<?php

namespace Cherry\AppBundle;

use Cherry\AppBundle\DependencyInjection\Compiler\BankCompilerPass;
use Cherry\AppBundle\DependencyInjection\Compiler\BonusFactoryCollectionCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CherryAppBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container
            ->addCompilerPass(new BankCompilerPass())
            ->addCompilerPass(new BonusFactoryCollectionCompilerPass());
    }

}
