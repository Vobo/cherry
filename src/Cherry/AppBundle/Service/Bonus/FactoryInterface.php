<?php

namespace Cherry\AppBundle\Service\Bonus;

use Cherry\AppBundle\Entity\Bonus;
use Cherry\AppBundle\Model\Bonus\BonusModelInterface;
use Symfony\Component\Form\Form;

interface FactoryInterface
{
    /**
     * @return string
     */
    public function supportedType();

    /**
     * @return Form
     */
    public function createFormType();

    /**
     * @param Bonus $bonus
     * @return BonusModelInterface
     */
    public function createModel(Bonus $bonus);

    /**
     * @return BonusModelInterface
     */
    public function createModelWithEntity();
}
