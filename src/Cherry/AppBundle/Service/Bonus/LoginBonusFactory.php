<?php

namespace Cherry\AppBundle\Service\Bonus;

use Cherry\AppBundle\Entity\Bonus;
use Cherry\AppBundle\Form\Bonus\LoginBonusType;
use Cherry\AppBundle\Model\Bonus\LoginBonusModel;

class LoginBonusFactory extends BaseFactory
{
    /**
     * {@inheritdoc}
     */
    public function supportedType()
    {
        return LoginBonusModel::TYPE;
    }

    /**
     * {@inheritdoc}
     */
    public function createFormType()
    {
        return new LoginBonusType();
    }

    /**
     * {@inheritdoc}
     */
    public function createModel(Bonus $bonus)
    {
        return new LoginBonusModel($bonus);
    }

}
