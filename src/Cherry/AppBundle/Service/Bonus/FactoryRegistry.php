<?php

namespace Cherry\AppBundle\Service\Bonus;

use Cherry\AppBundle\Entity\Bonus;
use Exception;

class FactoryRegistry
{
    /** @var FactoryInterface[] */
    protected $factories = [];

    /**
     * @param FactoryInterface $factory
     */
    public function registerFactory(FactoryInterface $factory)
    {
        $this->factories[$factory->supportedType()] = $factory;
    }

    /**
     * @param Bonus $bonus
     * @return FactoryInterface
     * @throws Exception
     */
    public function getFactoryForBonus(Bonus $bonus)
    {
        return $this->getFactoryForType($bonus->getType());
    }

    /**
     * @param string $type
     * @return FactoryInterface
     * @throws Exception
     */
    public function getFactoryForType($type)
    {
        if (!array_key_exists($type, $this->factories)) {
            throw new Exception(sprintf('No factory registered for "%s" bonus type yet.', $type));
        }

        return $this->factories[$type];
    }

}
