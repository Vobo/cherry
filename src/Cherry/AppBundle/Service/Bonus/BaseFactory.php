<?php

namespace Cherry\AppBundle\Service\Bonus;

use Cherry\AppBundle\Entity\Bonus;

abstract class BaseFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createModelWithEntity()
    {
        $bonus = new Bonus();
        $bonus->setType($this->supportedType());

        return $this->createModel($bonus);
    }

}
