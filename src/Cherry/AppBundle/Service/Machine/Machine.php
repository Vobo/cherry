<?php

namespace Cherry\AppBundle\Service\Machine;

use Cherry\AppBundle\Model\MoneyTransferModel;
use Cherry\AppBundle\Service\Model\GambleModelFactory;
use Exception;

class Machine implements MachineInterface
{
    /** @var GambleModelFactory */
    protected $gambleModelFactory;

    /** @var integer */
    protected $winMultiplier;

    /** @var integer */
    protected $chanceToWinInPercentage;

    public function __construct(
        GambleModelFactory $gambleModelFactory,
        $winMultiplier,
        $chanceToWinInPercentage
    ) {
        $this->gambleModelFactory = $gambleModelFactory;
        $this->winMultiplier = $winMultiplier;
        $this->chanceToWinInPercentage = $chanceToWinInPercentage;

        if (!is_int($winMultiplier) || $winMultiplier < 1 || $winMultiplier > 25) {
            throw new Exception('Change to win has to be an integer in 1-25 range.');
        }

        if (!is_int($chanceToWinInPercentage) || $chanceToWinInPercentage < 1 || $chanceToWinInPercentage > 99) {
            throw new Exception('Change to win has to be an integer in 1-99 range.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function gamble(MoneyTransferModel $wager)
    {
        $randomNumber = mt_rand(1, 100);

        if ($randomNumber < $this->chanceToWinInPercentage) {
            $wonMoney = $wager->getMoney()->multiply($this->winMultiplier)->subtract($wager->getMoney());

            return $this->gambleModelFactory->createWonModel($wager, $wonMoney);
        }

        return $this->gambleModelFactory->createLostModel($wager);
    }
}
