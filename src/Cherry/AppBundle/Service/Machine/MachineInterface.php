<?php

namespace Cherry\AppBundle\Service\Machine;

use Cherry\AppBundle\Model\GambleModel;
use Cherry\AppBundle\Model\MoneyTransferModel;

interface MachineInterface
{
    /**
     * @param MoneyTransferModel $wager
     * @return GambleModel
     */
    public function gamble(MoneyTransferModel $wager);
}
