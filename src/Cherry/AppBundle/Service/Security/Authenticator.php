<?php

namespace Cherry\AppBundle\Service\Security;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use Cherry\AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class Authenticator implements SimplePreAuthenticatorInterface, AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /** @var RouterInterface */
    protected $router;

    /** @var SelectedUserStorage */
    protected $selectedUserStorage;

    /** @var $flashMessage */
    protected $flashMessage;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        RouterInterface $router,
        SelectedUserStorage $selectedUserStorage,
        FlashMessage $flashMessage
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->router = $router;
        $this->selectedUserStorage = $selectedUserStorage;
        $this->flashMessage = $flashMessage;
    }

    /**
     * {@inheritdoc}
     */
    public function createToken(Request $request, $providerKey)
    {
        $user = $this->selectedUserStorage->getSelectedUser();

        if (!$user) {
            throw new BadCredentialsException('No user selected.');
        }

        return new PreAuthenticatedToken('anon.', $user->getUsername(), $providerKey);
    }

    /**
     * {@inheritdoc}
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $username = $token->getCredentials();
        /** @var User $user */
        $user = $userProvider->loadUserByUsername($username);

        return new PreAuthenticatedToken($user, $username, $providerKey, $user->getRoles());
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->flashMessage->success('Logged in!');

        $event = new InteractiveLoginEvent($request, $token);
        $this->eventDispatcher->dispatch(SecurityEvents::INTERACTIVE_LOGIN, $event);

        return new RedirectResponse($this->router->generate('cherry_app_simulation_index'));
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->flashMessage->error('Select user first!');

        return new RedirectResponse($this->router->generate('cherry_app_simulation_index'));
    }

}
