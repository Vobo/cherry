<?php

namespace Cherry\AppBundle\Service\Security;

use Cherry\AppBundle\Entity\User;

interface SelectedUserStorageInterface
{
    /**
     * @param User $user
     * @return void
     */
    public function setSelectedUser(User $user);

    /**
     * @return User|null
     */
    public function getSelectedUser();

    /**
     * @return int|null
     */
    public function getSelectedUserId();
}
