<?php

namespace Cherry\AppBundle\Service\Security;

use Cherry\AppBundle\Entity\User;
use Cherry\AppBundle\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class SelectedUserStorage implements SelectedUserStorageInterface
{
    const KEY_SESSION_USER = 'selected.user';

    /** @var Session */
    protected $session;

    /** @var UserRepository */
    protected $userRepository;

    public function __construct(
        Session $session,
        UserRepository $userRepository
    ) {
        $this->session = $session;
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function setSelectedUser(User $user)
    {
        $this->session->set(self::KEY_SESSION_USER, $user->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function getSelectedUser()
    {
        if (!$this->hasSelectedUser()) {
            return null;
        }

        $id = $this->getSelectedUserId();
        $user = $this->userRepository->find($id);

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getSelectedUserId()
    {
        if (!$this->hasSelectedUser()) {
            return null;
        }

        $id = $this->session->get(self::KEY_SESSION_USER);

        return $id;
    }

    /**
     * @return bool
     */
    public function hasSelectedUser()
    {
        if (!$this->session->has(self::KEY_SESSION_USER)) {
            return false;
        }

        $id = $this->session->get(self::KEY_SESSION_USER);
        $userExists = (bool) $this->userRepository->find($id);

        if (!$userExists) {
            $this->session->remove(self::KEY_SESSION_USER);
        }

        return $userExists;
    }
}
