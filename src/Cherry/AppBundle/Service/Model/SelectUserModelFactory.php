<?php

namespace Cherry\AppBundle\Service\Model;

use Cherry\AppBundle\Entity\User;
use Cherry\AppBundle\Model\SelectUserModel;

class SelectUserModelFactory
{
    /**
     * @param User $user
     * @return SelectUserModel
     */
    public function createModel(User $user = null)
    {
        $model = new SelectUserModel();

        if ($user) {
            $model->setUser($user);
        }

        return $model;
    }
}
