<?php

namespace Cherry\AppBundle\Service\Model;

use Cherry\AppBundle\Entity\User;
use Cherry\AppBundle\Model\GambleModel;
use Cherry\AppBundle\Model\MoneyTransferModel;
use Money\Money;

class GambleModelFactory
{
    /** @var MoneyTransferModelFactory  */
    protected $moneyTransferModelFactory;

    public function __construct(
        MoneyTransferModelFactory $moneyTransferModelFactory
    ) {
        $this->moneyTransferModelFactory = $moneyTransferModelFactory;
    }

    /**
     * @param MoneyTransferModel $wager
     * @param Money $winningsMoney
     * @return GambleModel
     */
    public function createWonModel(MoneyTransferModel $wager, Money $winningsMoney)
    {
        return GambleModel::won($wager, $winningsMoney);
    }

    /**
     * @param MoneyTransferModel $wager
     * @return GambleModel
     */
    public function createLostModel(MoneyTransferModel $wager)
    {
        return GambleModel::lost($wager);
    }

    /**
     * @param User $user
     * @param Money $wagerMoney
     * @param Money $winningsMoney
     * @return GambleModel
     */
    public function createWonModelForUser(User $user, Money $wagerMoney, Money $winningsMoney)
    {
        $wager = $this->moneyTransferModelFactory->createModelForUser($user, $wagerMoney);
        $gamble = $this->createWonModel($wager, $winningsMoney);

        return $gamble;
    }

    /**
     * @param User $user
     * @param Money $wagerMoney
     * @return GambleModel
     */
    public function createLostModelForUser(User $user, Money $wagerMoney)
    {
        $wager = $this->moneyTransferModelFactory->createModelForUser($user, $wagerMoney);
        $gamble = $this->createLostModel($wager);

        return $gamble;
    }
}
