<?php

namespace Cherry\AppBundle\Service\Model;

use Cherry\AppBundle\Entity\User;
use Cherry\AppBundle\Model\PortfolioModel;

class PortfolioModelFactory
{
    /**
     * @param User $user
     * @return PortfolioModel
     */
    public function createModel(User $user)
    {
        $model = new PortfolioModel($user);

        return $model;
    }
}
