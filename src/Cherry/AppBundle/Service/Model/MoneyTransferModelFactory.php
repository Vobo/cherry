<?php

namespace Cherry\AppBundle\Service\Model;

use Cherry\AppBundle\Entity\User;
use Cherry\AppBundle\Model\PortfolioModel;
use Cherry\AppBundle\Model\MoneyTransferModel;
use Money\Money;

class MoneyTransferModelFactory
{
    /** @var PortfolioModelFactory */
    protected $portfolioModelFactory;

    public function __construct(
        PortfolioModelFactory $portfolioModelFactory
    ) {
        $this->portfolioModelFactory = $portfolioModelFactory;
    }

    /**
     * @param PortfolioModel $portfolio
     * @param Money $money
     * @return MoneyTransferModel
     */
    public function createModel(PortfolioModel $portfolio, Money $money = null)
    {
        $model = new MoneyTransferModel($portfolio);

        if ($money) {
            $model->setMoney($money);
        }

        return $model;
    }

    /**
     * @param User $user
     * @param Money $money
     * @return MoneyTransferModel
     */
    public function createModelForUser(User $user, Money $money = null)
    {
        $portfolio = $this->portfolioModelFactory->createModel($user);
        $model = $this->createModel($portfolio, $money);

        return $model;
    }
}
