<?php

namespace Cherry\AppBundle\Service\Bank\GambleHandler;

use Cherry\AppBundle\Model\GambleModel;
use Cherry\AppBundle\Service\Bank\GambleHandler\Helper\AssertGambleSupportedTrait;

class MoneyWonHandler implements GambleHandlerInterface
{
    use AssertGambleSupportedTrait;

    /**
     * {@inheritdoc}
     */
    public function supports(GambleModel $gamble)
    {
        $wageredMoney = $gamble->getWageredMoney();
        $money = $gamble->getPortfolio()->getMoney();

        return $gamble->isWon() && ($wageredMoney->equals($money) || $wageredMoney->lessThan($money));
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GambleModel $gamble)
    {
        $this->assertGambleSupported($gamble);

        $moneyWallet = $gamble->getPortfolio()->getMoneyWallet();
        $wonMoney = $gamble->getWinnings();
        $moneyWallet->setCurrentMoney($moneyWallet->getCurrentMoney()->add($wonMoney));
    }

}
