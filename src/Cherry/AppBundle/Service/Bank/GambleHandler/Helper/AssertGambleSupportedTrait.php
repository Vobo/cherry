<?php

namespace Cherry\AppBundle\Service\Bank\GambleHandler\Helper;

use Cherry\AppBundle\Model\GambleModel;
use Exception;

trait AssertGambleSupportedTrait
{
    abstract protected function supports(GambleModel $gamble);

    /**
     * @param GambleModel $gamble
     * @throws Exception
     * @return void
     */
    protected function assertGambleSupported(GambleModel $gamble)
    {
        if (!$this->supports($gamble)) {
            throw new Exception('This handler cannot support passed wager outcome.');
        }
    }
}
