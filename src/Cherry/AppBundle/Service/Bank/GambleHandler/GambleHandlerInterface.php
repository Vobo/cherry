<?php

namespace Cherry\AppBundle\Service\Bank\GambleHandler;

use Cherry\AppBundle\Model\GambleModel;

interface GambleHandlerInterface
{
    /**
     * @param GambleModel $gamble
     * @return bool
     */
    public function supports(GambleModel $gamble);

    /**
     * @param GambleModel $gamble
     * @return void
     */
    public function handle(GambleModel $gamble);
}
