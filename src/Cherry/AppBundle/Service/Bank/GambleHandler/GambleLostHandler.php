<?php

namespace Cherry\AppBundle\Service\Bank\GambleHandler;

use Cherry\AppBundle\Entity\Wallet;
use Cherry\AppBundle\Model\GambleModel;
use Cherry\AppBundle\Service\Bank\GambleHandler\Helper\AssertGambleSupportedTrait;
use Exception;
use Money\Money;

class GambleLostHandler implements GambleHandlerInterface
{
    use AssertGambleSupportedTrait;

    /**
     * {@inheritdoc}
     */
    public function supports(GambleModel $gamble)
    {
        return !$gamble->isWon();
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GambleModel $gamble)
    {
        $this->assertGambleSupported($gamble);

        $moneyLeftToWithdraw = $gamble->getWageredMoney();
        $moneyWallet = $gamble->getPortfolio()->getMoneyWallet();
        $moneyLeftToWithdraw = $this->withdrawMoneyFromWallet($moneyWallet, $moneyLeftToWithdraw);

        if ($moneyLeftToWithdraw->isZero()) {
            return;
        }

        // Start from newest bonus wallets
        $bonusWallets = $gamble->getPortfolio()->getBonusActiveWallets()->toArray();
        $bonusWallets = array_reverse($bonusWallets);

        foreach ($bonusWallets as $bonusWallet) {
            $moneyLeftToWithdraw = $this->withdrawMoneyFromWallet($bonusWallet, $moneyLeftToWithdraw);

            if ($moneyLeftToWithdraw->isZero()) {
                return;
            }
        }

        throw new Exception(sprintf(
            'Not enough money in portfolio to withdraw lost wager - wagered %d, missing %d to withdraw lost amount.',
            $gamble->getWageredMoney()->getAmount(),
            $moneyLeftToWithdraw->getAmount()
        ));
    }

    /**
     * @param Wallet $wallet
     * @param Money $moneyLeftToWithdraw
     * @return Money money left to take after withdrawing it from passed wallet
     */
    protected function withdrawMoneyFromWallet(Wallet $wallet, Money $moneyLeftToWithdraw)
    {
        $moneyToTake = $moneyLeftToWithdraw->greaterThan($wallet->getCurrentMoney())
            ? $wallet->getCurrentMoney()
            : $moneyLeftToWithdraw;
        $wallet->setCurrentMoney($wallet->getCurrentMoney()->subtract($moneyToTake));
        $moneyLeftToWithdraw = $moneyLeftToWithdraw->subtract($moneyToTake);

        return $moneyLeftToWithdraw;
    }

}
