<?php

namespace Cherry\AppBundle\Service\Bank\GambleHandler;

use Cherry\AppBundle\Entity\BonusWallet;
use Cherry\AppBundle\Model\GambleModel;
use Cherry\AppBundle\Service\Bank\GambleHandler\Helper\AssertGambleSupportedTrait;

class BonusMoneyWonHandler implements GambleHandlerInterface
{
    use AssertGambleSupportedTrait;

    /**
     * {@inheritdoc}
     */
    public function supports(GambleModel $gamble)
    {
        $wageredMoney = $gamble->getWageredMoney();
        $money = $gamble->getPortfolio()->getMoney();

        return $gamble->isWon() && $wageredMoney->greaterThan($money);
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GambleModel $gamble)
    {
        $this->assertGambleSupported($gamble);

        $moneyToDistribute = $gamble->getWinnings();

        // Start from newest bonus wallets
        $bonusWallets = $gamble->getPortfolio()->getBonusActiveWallets()->toArray();
        $bonusWallets = array_reverse($bonusWallets);

        /** @var BonusWallet $bonusWallet */
        foreach ($bonusWallets as $bonusWallet) {
            $capacityLeft = $bonusWallet->getInitialMoney()->subtract($bonusWallet->getCurrentMoney());

            $moneyToTransfer = $capacityLeft->greaterThan($moneyToDistribute)
                ? $moneyToDistribute
                : $capacityLeft;

            $moneyToDistribute = $moneyToDistribute->subtract($moneyToTransfer);
            $bonusWallet->setCurrentMoney($bonusWallet->getCurrentMoney()->add($moneyToTransfer));

            if ($moneyToDistribute->isZero()) {
                return;
            }
        }

        $moneyWallet = $gamble->getPortfolio()->getMoneyWallet();
        $moneyWallet->setCurrentMoney($moneyWallet->getCurrentMoney()->add($moneyToDistribute));
    }

}
