<?php

namespace Cherry\AppBundle\Service\Bank;

use Cherry\AppBundle\Model\GambleModel;
use Cherry\AppBundle\Model\MoneyTransferModel;
use Cherry\AppBundle\Service\Bank\Event\BankDepositEvent;
use Cherry\AppBundle\Service\Bank\Event\BankEvents;
use Cherry\AppBundle\Service\Bank\Event\BankGambleEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventDispatchingBank implements BankInterface
{
    /** @var BankInterface */
    protected $baseBank;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        BankInterface $baseBank,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->baseBank = $baseBank;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function depositMoney(MoneyTransferModel $deposit)
    {
        $this->baseBank->depositMoney($deposit);

        $event = new BankDepositEvent($deposit);
        $this->eventDispatcher->dispatch(BankEvents::EVENT_DEPOSITED_MONEY, $event);
    }

    /**
     * {@inheritdoc}
     */
    public function processGamble(GambleModel $gamble)
    {
        $this->baseBank->processGamble($gamble);

        $event = new BankGambleEvent($gamble);
        $this->eventDispatcher->dispatch(BankEvents::EVENT_PROCESSED_GAMBLE, $event);
    }

}
