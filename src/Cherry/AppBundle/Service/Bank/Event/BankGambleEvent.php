<?php

namespace Cherry\AppBundle\Service\Bank\Event;

use Cherry\AppBundle\Model\GambleModel;
use Symfony\Component\EventDispatcher\Event;

class BankGambleEvent extends Event
{
    /** @var GambleModel */
    protected $gamble;

    public function __construct(GambleModel $gamble)
    {
        $this->gamble = $gamble;
    }

    /**
     * @return GambleModel
     */
    public function getGamble()
    {
        return $this->gamble;
    }

}
