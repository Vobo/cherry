<?php

namespace Cherry\AppBundle\Service\Bank\Event;

class BankEvents 
{
    const EVENT_DEPOSITED_MONEY = 'cherry_app_bundle.bank.deposited_money';
    const EVENT_PROCESSED_GAMBLE = 'cherry_app_bundle.bank.gamble';
}
