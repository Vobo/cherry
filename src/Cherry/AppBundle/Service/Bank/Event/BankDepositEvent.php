<?php

namespace Cherry\AppBundle\Service\Bank\Event;

use Cherry\AppBundle\Model\MoneyTransferModel;
use Symfony\Component\EventDispatcher\Event;

class BankDepositEvent extends Event
{
    /** @var MoneyTransferModel */
    protected $deposit;

    public function __construct(MoneyTransferModel $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * @return MoneyTransferModel
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

}
