<?php

namespace Cherry\AppBundle\Service\Bank;

use Cherry\AppBundle\Model\GambleModel;
use Cherry\AppBundle\Model\MoneyTransferModel;

interface BankInterface
{
    /**
     * @param MoneyTransferModel $deposit
     * @return void
     */
    public function depositMoney(MoneyTransferModel $deposit);

    /**
     * @param GambleModel $gamble
     * @return void
     */
    public function processGamble(GambleModel $gamble);
}
