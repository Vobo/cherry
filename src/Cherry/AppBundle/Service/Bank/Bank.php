<?php

namespace Cherry\AppBundle\Service\Bank;

use Cherry\AppBundle\Model\GambleModel;
use Cherry\AppBundle\Model\MoneyTransferModel;
use Cherry\AppBundle\Service\Bank\GambleHandler\GambleHandlerInterface;
use Cherry\AppBundle\Service\Model\GambleModelFactory;
use Exception;
use Money\Money;

class Bank implements BankInterface
{
    /** @var GambleModelFactory */
    protected $gambleModelFactory;

    /** @var GambleHandlerInterface[] */
    protected $gambleHandlers = [];

    public function __construct(GambleModelFactory $gambleModelFactory) {
        $this->gambleModelFactory = $gambleModelFactory;
    }

    /**
     * @param GambleHandlerInterface $gambleHandler
     */
    public function registerHandler(GambleHandlerInterface $gambleHandler)
    {
        $this->gambleHandlers[] = $gambleHandler;
    }

    /**
     * {@inheritdoc}
     */
    public function depositMoney(MoneyTransferModel $deposit)
    {
        // Treat deposit as a gamble without any wager.
        $gamble = $this->gambleModelFactory->createWonModelForUser(
            $deposit->getPortfolio()->getUser(),
            new Money(0, $deposit->getPortfolio()->getMoney()->getCurrency()),
            $deposit->getMoney());

        $this->handleGamble($gamble);
        // @todo events - create bonus wallet on trigger
    }

    /**
     * {@inheritdoc}
     */
    public function processGamble(GambleModel $gamble)
    {
        $this->handleGamble($gamble);
    }

    /**
     * @param GambleModel $gamble
     * @throws Exception
     */
    protected function handleGamble(GambleModel $gamble)
    {
        foreach ($this->gambleHandlers as $handler) {
            if (!$handler->supports($gamble)) {
                continue;
            }

            $handler->handle($gamble);

            return;
        }

        throw new Exception('No handler found.');
    }

}
